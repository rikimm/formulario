import { Component, OnInit } from '@angular/core';
import{ NgForm } from "@angular/forms"

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
	disable = true;
	usuario:Object={
		nombre:null,
		apellidos:null,
		mail:null,
		contrasena:null
	}
	tabla:Array<string>=[]
  	constructor() { }

  	ngOnInit() {
  	}
  	formulario( form:NgForm){
  		if(form.valid){
  			this.tabla.push(form.value)
  		}
  	}
}
